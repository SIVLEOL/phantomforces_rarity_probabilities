# Simulate phantom forces crate rolls to estimate true probability.
#
# Written by SIVLEOL in October 2017.

import random
from datetime import datetime
start = datetime.now()

def main(max_rolls=1000, reddit_table_format=0):

    #Initial and current roll probabilities.
    starting_chance_dict = {"Common":0.60, "Uncommon": 0.26, "Rare":0.10, "Very Rare":0.03, "Legendary": 0.01}
    chance_dict = {"Common":0.60, "Uncommon": 0.26, "Rare":0.10, "Very Rare":0.03, "Legendary": 0.01}
    
    #Probability changes after each roll, unless a "Very Rare" or "Legendary" is rolled.
    change_dict = {"Common":-0.0050, "Uncommon":-0.0025, "Rare":0.0, "Very Rare":0.0050, "Legendary": 0.0025}
    
    count_dict = {"Common":0, "Uncommon": 0, "Rare":0, "Very Rare":0, "Legendary": 0}
    
    roll_count = 0
    
    while roll_count < max_rolls:
    
        random_number = random.random()
        
        additional_number = 0 #For incrementing each probability for algorithm to work.
        
        for element in ["Common", "Uncommon", "Rare", "Very Rare", "Legendary"]:
            if random_number < (chance_dict[element] + additional_number):
                count_dict[element] += 1
                
                #Update chance_dict
                if element == "Very Rare" or element == "Legendary": #Reset 
                    for rarity in chance_dict:
                        chance_dict[rarity] = starting_chance_dict[rarity]
                else:
                    for rarity in chance_dict: #Change
                        chance_dict[rarity] += change_dict[rarity]
                
                break
            additional_number += chance_dict[element]
                    
        roll_count += 1
    
    #Print results.
    probability_dict = dict()
        
    #Get probabilities.    
    for element in count_dict:
        probability_dict[element] = float(count_dict[element]) / float(max_rolls) #Use floating point division.
    
    for element in ["Common", "Uncommon", "Rare", "Very Rare", "Legendary"]:
        print "%-11s %d (%g%%)" % (element+":", count_dict[element], probability_dict[element]*100)
        
    if reddit_table_format:
        print "Rarity|Chance|Rolls\n:--|:--|:--"
        for element in ["Common", "Uncommon", "Rare", "Very Rare", "Legendary"]:
            print "%s|%g%%|%d" % (element+":", probability_dict[element]*100, count_dict[element])        
    
if __name__ == "__main__":
    main(max_rolls=10000000, reddit_table_format=1) #10 million rolls took my i7 processor 29.999000 seconds to run.
    print "Finished in:", datetime.now()-start